#! ruby
require 'csv'
require 'date'
require 'json'

NUMBER_OF_CUSTOMERS = 10
NUMBER_OF_INVOICES_PER_DAY = 5
NUMBER_OF_ITEMS = 10
TERM_SCHEDULES = [21, 30, 60, 90]
PAID_PERCENTAGE = [0, 0.25, 0.5, 1]

def generate_ggp(number_of_rows)
  column_headers = %w(id customer amount date adjustment term_schedule items billing_address shipping_address paid_at paid_amount note)
  file_name = "data-golden-gate-produce-#{number_of_rows}.csv"
  CSV.open(file_name, 'w', :write_headers => true, :headers => column_headers) do |csv|
    1.upto(number_of_rows).each do |row_index|
      csv << generate_row_ggp(row_index, number_of_rows)
    end
  end
  file_name
end

def generate_row_ggp(index, number_of_rows)
  row = []
  prng = Random.new
  date = Date.new(2022, 01, 01)
  row << "id-#{index}" #id
  customer_id = prng.rand(NUMBER_OF_CUSTOMERS)
  row << "customer-#{customer_id}" #customer
  total_amount = index * 100.0
  row << total_amount # amount
  invoice_date = date - (number_of_rows - index) / NUMBER_OF_INVOICES_PER_DAY
  row << invoice_date #date
  adjustment = prng.rand(0..1) * prng.rand(-100..100)
  adjustment = nil if adjustment == 0
  row << adjustment #adjustment
  term = TERM_SCHEDULES[prng.rand(4)]
  row << term #term_schedule
  item_id = prng.rand(NUMBER_OF_ITEMS)
  items = { items: [
    {
      item: "item-name-#{item_id}",
      quantity: index,
      unitPrice: 100.00,
      totalAmount: index * 100.00,
      description: "item-description-#{item_id}" }
  ] }
  row << items.to_json
  billing_address = "#{customer_id} Market Street, San Francisco CA 94102"
  row << billing_address
  shipping_address = "#{customer_id} Main Street, Oakland CA 94345"
  row << shipping_address
  paid_amount = total_amount * PAID_PERCENTAGE[prng.rand(4)]
  paid_at = paid_amount == 0 ? nil :  invoice_date + prng.rand(term + 7) # May be paid after due date
  paid_amount = paid_amount == 0 ? nil : paid_amount
  row << paid_at
  row << paid_amount
  note = prng.rand(0..20) == 0 ? "VOIDED" : "some-other-note-#{index}"
  row << note
end

def generate_hf(number_of_rows)
  column_headers = %w(invoiceId transAmountCents transDate adjustmentAmountCents paymentTerm lines buyer billingAddress shippingAddress paidAmountCents paidAt memo)
  file_name = "data-happy-fruits-#{number_of_rows}.csv"
  CSV.open(file_name, 'w', :write_headers => true, :headers => column_headers) do |csv|
    1.upto(number_of_rows).each do |row_index|
      csv << generate_row_hf(row_index, number_of_rows)
    end
  end
  file_name
end

def generate_row_hf(index, number_of_rows)
  row = []
  prng = Random.new
  date = Date.new(2022, 01, 01)
  row << "hf-#{index}" #id
  total_amount = index * 10000
  row << total_amount # amount
  invoice_date = date - (number_of_rows - index) / NUMBER_OF_INVOICES_PER_DAY
  row << invoice_date #date
  adjustment = prng.rand(0..1) * prng.rand(-1000..1000)
  adjustment = nil if adjustment == 0
  row << adjustment #adjustment
  term = TERM_SCHEDULES[prng.rand(4)]
  row << term #term_schedule
  item_id = prng.rand(NUMBER_OF_ITEMS)
  items = { lines: [
    {
      product: "product-name-#{item_id}",
      quantity: index,
      price: 10000,
      total: index * 10000,
      description: "line-description-#{item_id}" }
  ] }
  row << items.to_json
  customer_id = prng.rand(NUMBER_OF_CUSTOMERS)
  row << "buyer-#{customer_id}" #customer
  billing_address = "#{customer_id} Market Street, San Francisco CA 94102"
  row << billing_address
  shipping_address = "#{customer_id} Main Street, Oakland CA 94345"
  row << shipping_address
  paid_amount = total_amount * PAID_PERCENTAGE[prng.rand(4)]
  paid_at = paid_amount == 0 ? nil :  invoice_date + prng.rand(term + 7) # May be paid after due date
  paid_amount = paid_amount == 0 ? nil : paid_amount
  row << paid_amount
  row << paid_at
  note = prng.rand(0..20) == 0 ? "VOIDED" : "some-other-memo-#{index}"
  row << note
end

def read_a_row (file_name)
  rows = CSV.read(file_name)
  print rows[1]
  puts
end

# file_name = generate_ggp(10)
# read_a_row(file_name)
# file_name = generate_ggp(1000)
# read_a_row(file_name)
# file_name = generate_hf(10)
# read_a_row(file_name)
# file_name = generate_hf(1000)
# read_a_row(file_name)
